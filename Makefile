# ifeq ($(ZERO_DELAY), 1)
# OPTS += -DZERO_DELAY
# endif

.PHONY: all clean

all: libams035.a

ams035.o: ams035.hpp ams035.cpp $(SIMULATION_PARAMETERS_PATH)/simulation_parameters.hpp
	$(CXX) ams035.cpp -std=c++11 `pkg-config --libs --cflags logitherm` -I $(SIMULATION_PARAMETERS_PATH) -c -o ams035.o

libams035.a: ams035.o
	$(AR) $(ARFLAGS) libams035.a ams035.o
	
clean:
	rm -f ams035.o
	rm -f libams035.a
