#include "ams035.hpp"
#include <algorithm>
#include "simulation_parameters.hpp"

namespace ams035
{
	inline unit::energy_t interpolate(const unit::temperature_t abs_temp, const unit::energy_t array[])
	{
		unit::temperature_t temp = abs_temp - 273.15_K;
		/** a mert karakterisztikak 0 -- 80 C kozott ervenyesek **/
		if((0_celsius < temp) && (temp < 80_celsius))
		{
			size_t index = static_cast<double>(temp)/10;
			/** interpolalunk a ket legkozelebbi pont kozott**/
			unit::energy_t gradient = (array[index+1]-array[index]) / 10;
			unit::energy_t result = gradient*fmod(static_cast<double>(temp),10.0) + array[index];
			if(result < 0_J) throw("Negative dissipation");
			return result;
		}
		else return (temp < 0_celsius) ? array[0] : array[8];
	}
	
	inline unit::current_t interpolate(const unit::temperature_t abs_temp, const unit::current_t array[])
	{
		unit::temperature_t temp = abs_temp - 273.15_K;
		/** a mert karakterisztikak 0 -- 80 C kozott ervenyesek **/
		if((0_celsius < temp) && (temp < 80_celsius))
		{
			size_t index = static_cast<double>(temp)/10;
			/** interpolalunk a ket legkozelebbi pont kozott**/
			unit::current_t gradient = (array[index+1]-array[index]) / 10;
			unit::current_t result = gradient*fmod(static_cast<double>(temp),10.0) + array[index];
			if(result < 0_A) throw("Negative current");
			return result;
		}
		else return (temp < 0_celsius) ? array[0] : array[8];
	}
	
	inline unit::time_t interpolate(const unit::temperature_t abs_temp, const unit::time_t array[])
	{
		unit::temperature_t temp = abs_temp - 273.15_K;
		/** a mert karakterisztikak 0 -- 80 C kozott ervenyesek **/
		if((0_celsius < temp) && (temp < 80_celsius))
		{
			size_t index = static_cast<double>(temp)/10;
			/** interpolalunk a ket legkozelebbi pont kozott**/
			unit::time_t gradient = (array[index+1]-array[index]) / 10;
			unit::time_t result = gradient*fmod(static_cast<double>(temp),10.0) + array[index];
			if(result < 0_s) throw("Negative delay");
			return result;
		}
		else return (temp < 0_celsius) ? array[0] : array[8];
	}
	
	const unit::energy_t nand20::q_pos_energy[9]
	{	
		0.1166986973224975_pJ,
		0.1164115805209295_pJ,
		0.1161577018865896_pJ,
		0.1159390287041531_pJ,
		0.1157530742757361_pJ,
		0.1155862835320505_pJ,
		0.1154524862317898_pJ,
		0.11534129133036_pJ,
		0.1152079363797866_pJ
	};
	
	const unit::energy_t nand20::q_neg_energy[9]
	{
		0.1064296607229646_pJ,
		0.1059725369175862_pJ,
		0.1055692928794947_pJ,
		0.1052291104079182_pJ,
		0.1049203899876013_pJ,
		0.1046812611523468_pJ,
		0.1044701988555406_pJ,
		0.1042982911635377_pJ,
		0.1041759655197528_pJ
	};
	
	const unit::current_t nand20::leakage_current[9]
	{
		1.654772673053529_pA,
		1.662005267858254_pA,
		1.678330832788449_pA,
		1.712932597297782_pA,
		1.781819068553048_pA,
		1.910158879817652_pA,
		2.132085816022355_pA,
		2.48392840766333_pA,
		2.986125593074907_pA
	};
	
	const unit::time_t nand20::q_pos_delay[9]
	{
		0.370571836270020_ns,
		0.373946430437014_ns,
		0.377202847864014_ns,
		0.380344323919042_ns,
		0.383374793649084_ns,
		0.386298016337025_ns,
		0.389128982420026_ns,
		0.391868025931032_ns,
		0.394479510429056_ns
	};
	
	const unit::time_t nand20::q_neg_delay[9]
	{
		0.1954901010400_ns,
		0.2016612116596_ns,
		0.2077528354995_ns,
		0.2137649812796_ns,
		0.2198014526597_ns,
		0.2256836573201_ns,
		0.2315844448497_ns,
		0.2374652508199_ns,
		0.2432192652799_ns
	};

	void nand20::function()
	{
		q_delay.write(~(A.read() & B.read()));
	}

	sc_time nand20::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t nand20::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t nand20::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t nand20::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}

	nand20::nand20(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&nand20::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&nand20::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(3.5_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(nand20);
		
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&nand20::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
		
	}
	
	const unit::energy_t nand30::q_pos_energy[9]
	{	
		0.1262646264010353_pJ,
		0.1260023302569394_pJ,
		0.1257432340204353_pJ,
		0.125550673556815_pJ,
		0.1253883961442374_pJ,
		0.1252669512095187_pJ,
		0.1251012951437133_pJ,
		0.1250123391934522_pJ,
		0.1249287433920207_pJ
	};
	
	const unit::energy_t nand30::q_neg_energy[9]
	{
		0.1217629419393029_pJ,
		0.1214167007886305_pJ,
		0.121156759409378_pJ,
		0.1209684953376925_pJ,
		0.1208513392627295_pJ,
		0.1208318516696904_pJ,
		0.1208651810842544_pJ,
		0.1209270069676748_pJ,
		0.1210240105243648_pJ
	};
	
	const unit::current_t nand30::leakage_current[9]
	{
		1.103095913223024_pA,
		1.107862515581731_pA,
		1.118760447285884_pA,
		1.142062541706784_pA,
		1.188556537671737_pA,
		1.274453649353954_pA,
		1.419315770843858_pA,
		1.661016446249615_pA,
		2.239395111333748_pA
	};
	
	const unit::time_t nand30::q_pos_delay[9]
	{
		0.3999019485479_ns,
		0.4038549545830_ns,
		0.4076786536031_ns,
		0.4113883722470_ns,
		0.4149954188850_ns,
		0.4184829219850_ns,
		0.4218624382441_ns,
		0.4251468413880_ns,
		0.4283265633690_ns
	};
	
	const unit::time_t nand30::q_neg_delay[9]
	{
		0.2298033480410_ns,
		0.2368249015702_ns,
		0.2438135689707_ns,
		0.2507293741210_ns,
		0.2576119338804_ns,
		0.2644004968105_ns,
		0.2711652877804_ns,
		0.2779029799006_ns,
		0.2845470170607_ns
	};
	
	
	void nand30::function()
	{
		q_delay.write(~(A.read() & B.read() & C.read()));
	}

	sc_time nand30::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t nand30::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t nand30::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t nand30::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}

	nand30::nand30(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&nand30::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&nand30::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(nand30);

		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&nand30::q_delay_func, this));
		}
	
		SC_METHOD(function);
		sensitive << A << B << C;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t nand40::q_pos_energy[9]
	{	
		0.1347374369602163_pJ,
		0.1345271548385668_pJ,
		0.1343534661400495_pJ,
		0.1341915012780828_pJ,
		0.1340943094293183_pJ,
		0.1339480368725886_pJ,
		0.1338770003495238_pJ,
		0.133863688419429_pJ,
		0.1338608865274353_pJ
	};
	
	const unit::energy_t nand40::q_neg_energy[9]
	{
		0.1374205383785784_pJ,
		0.1375581242349653_pJ,
		0.1377698743726211_pJ,
		0.138031911877297_pJ,
		0.138319991432983_pJ,
		0.1386265281931248_pJ,
		0.1389467126051245_pJ,
		0.139294226727672_pJ,
		0.1396263406458676_pJ
	};
	const unit::current_t nand40::leakage_current[9]
	{
		0.8273199833538084_pA,
		0.8309627330776031_pA,
		0.8393557209962104_pA,
		0.8574057439113008_pA,
		0.8934611656492661_pA,
		0.9596081540039501_pA,
		1.070284132492002_pA,
		1.335309977982384_pA,
		1.755641252682154_pA
	};
	
	const unit::time_t nand40::q_pos_delay[9]
	{
		0.4255187009880_ns,
		0.4300484282689_ns,
		0.4344390152970_ns,
		0.4387007893500_ns,
		0.4428538370030_ns,
		0.4468927535220_ns,
		0.4508604165280_ns,
		0.4546859454120_ns,
		0.4584163896300_ns
	};
	
	const unit::time_t nand40::q_neg_delay[9]
	{
		0.2666827290581_ns,
		0.2748066706800_ns,
		0.2828770531482_ns,
		0.2908836428682_ns,
		0.2988687170383_ns,
		0.3067790060088_ns,
		0.3146106153390_ns,
		0.3224239399892_ns,
		0.3301629038085_ns
	};
	
	void nand40::function()
	{
		q_delay.write(~(A.read() & B.read() & C.read() & D.read()));
	}

	sc_time nand40::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t nand40::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t nand40::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t nand40::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}

	nand40::nand40(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&nand40::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&nand40::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(nand40);

		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&nand40::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << C << D;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t nor20::q_pos_energy[9]
	{	
		0.1444297050163989_pJ,
		0.1442246814070553_pJ,
		0.144068922424213_pJ,
		0.1439481414564039_pJ,
		0.1438585800441884_pJ,
		0.1438103876605913_pJ,
		0.143782400626641_pJ,
		0.1437837166430734_pJ,
		0.1438060750734068_pJ
	};
	
	const unit::energy_t nor20::q_neg_energy[9]
	{
		0.1100523991497432_pJ,
		0.1094342471319595_pJ,
		0.1088506936059332_pJ,
		0.1082856955552578_pJ,
		0.1077601980637695_pJ,
		0.1072346391636163_pJ,
		0.1067475210179592_pJ,
		0.1062996332163671_pJ,
		0.1058696685008072_pJ
	};
	
	const unit::current_t nor20::leakage_current[9]
	{
		6.623377193924585_pA,
		6.658040685409007_pA,
		6.734279847991058_pA,
		6.893972097344283_pA,
		7.212182354465076_pA,
		7.818247813404554_pA,
		8.926155786964521_pA,
		10.87718888937426_pA,
		14.19790346014944_pA
	};
	
	const unit::time_t nor20::q_pos_delay[9]
	{
		0.2891911920699_ns,
		0.2923451570500_ns,
		0.2954083265298_ns,
		0.2983844546499_ns,
		0.3012520992201_ns,
		0.3040500428500_ns,
		0.3067604206800_ns,
		0.3093821605900_ns,
		0.3118981539601_ns
	};
	
	const unit::time_t nor20::q_neg_delay[9]
	{
		0.06771452236966_ns,
		0.07196189926952_ns,
		0.07609941367990_ns,
		0.08014148847000_ns,
		0.08418892523954_ns,
		0.08819942428968_ns,
		0.09224511098963_ns,
		0.09618815798006_ns,
		0.1000963923699_ns
	};
	
	void nor20::function()
	{
		q_delay.write(~(A.read() | B.read()));
	}

	sc_time nor20::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t nor20::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t nor20::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t nor20::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
	
	nor20::nor20(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&nor20::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&nor20::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(3.5_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))		
	{
		SC_HAS_PROCESS(nor20);

		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&nor20::q_delay_func, this));
		}

		SC_METHOD(function);
		sensitive << A << B;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}

	const unit::energy_t nor30::q_pos_energy[9]
	{	
		0.1595313636349125_pJ,
		0.15930264392059_pJ,
		0.1591206920317402_pJ,
		0.1589786175124584_pJ,
		0.1588565346009697_pJ,
		0.1587665913331177_pJ,
		0.1587154504387977_pJ,
		0.1586805095560986_pJ,
		0.1586688226675052_pJ
	};
	
	const unit::energy_t nor30::q_neg_energy[9]
	{
		0.1425047736133434_pJ,
		0.1417347518651024_pJ,
		0.1408328322512572_pJ,
		0.1400643478125264_pJ,
		0.1393366153234027_pJ,
		0.1386019624690171_pJ,
		0.1379135498116345_pJ,
		0.1372099255272373_pJ,
		0.1365230004893569_pJ
	};
	
	const unit::current_t nor30::leakage_current[9]
	{
		9.935413618864404_pA,
		9.987072403287238_pA,
		10.10143114339442_pA,
		10.34096951156329_pA,
		10.81828488710331_pA,
		11.72738305667615_pA,
		13.38924498020223_pA,
		16.31579455848411_pA,
		21.29686625342114_pA
	};
	
	const unit::time_t nor30::q_pos_delay[9]
	{
		0.3017291702698_ns,
		0.3038085711002_ns,
		0.3057580445003_ns,
		0.3076303837298_ns,
		0.3093803613798_ns,
		0.3110292576199_ns,
		0.3125797391703_ns,
		0.3140363347698_ns,
		0.3153844823799_ns
	};
	
	const unit::time_t nor30::q_neg_delay[9]
	{
		0.04106674708020_ns,
		0.04493445280068_ns,
		0.04898803871050_ns,
		0.05288522570070_ns,
		0.05674224704076_ns,
		0.06051385993032_ns,
		0.06431044795055_ns,
		0.06807184997058_ns,
		0.07179642336060_ns
	};

	void nor30::function()
	{
		q_delay.write(~(A.read() | B.read() | C.read()));
	}

	sc_time nor30::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t nor30::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t nor30::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t nor30::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}

	nor30::nor30(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&nor30::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&nor30::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(nor30);

		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&nor30::q_delay_func, this));
		}

		SC_METHOD(function);
		sensitive << A << B << C;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t nor40::q_pos_energy[9]
	{	
		0.2663580255485165_pJ,
		0.2671847478344462_pJ,
		0.2680406698989935_pJ,
		0.2689335940480108_pJ,
		0.2698622242438933_pJ,
		0.2707928357381255_pJ,
		0.2717312146842833_pJ,
		0.2726924031373947_pJ,
		0.2736703436420937_pJ
	};
	
	const unit::energy_t nor40::q_neg_energy[9]
	{
		0.1887914038986759_pJ,
		0.1876714693602192_pJ,
		0.1865988922196811_pJ,
		0.1855464591208849_pJ,
		0.1846170389697974_pJ,
		0.1837315260599634_pJ,
		0.1827782696730669_pJ,
		0.1819034909917905_pJ,
		0.1810289370638208_pJ
	};
	
	const unit::current_t nor40::leakage_current[9]
	{
		13.24728294204832_pA,
		13.3161613176419_pA,
		13.46863963275891_pA,
		13.78802411583005_pA,
		14.424444603018_pA,
		15.63657547064809_pA,
		17.8523913195626_pA,
		21.75445732342455_pA,
		28.39588603488839_pA
	};
	
	const unit::time_t nor40::q_pos_delay[9]
	{
		0.3881210475588_ns,
		0.3964372450497_ns,
		0.4046626274594_ns,
		0.4127835944794_ns,
		0.4206866844491_ns,
		0.4285855676093_ns,
		0.4361355455090_ns,
		0.4438139634996_ns,
		0.4513462353394_ns
	};
	
	const unit::time_t nor40::q_neg_delay[9]
	{
		0.03096028761901_ns,
		0.03489636515882_ns,
		0.03877759024935_ns,
		0.04259306295893_ns,
		0.04637391034059_ns,
		0.05016167158972_ns,
		0.05385220290770_ns,
		0.05750064059810_ns,
		0.06110300839012_ns
	};
	
	void nor40::function()
	{
		q_delay.write(~(A.read() | B.read() | C.read() | D.read()));
	}

	sc_time nor40::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t nor40::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t nor40::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t nor40::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}

	nor40::nor40(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&nor40::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&nor40::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))		
	{
		SC_HAS_PROCESS(nor40);

		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&nor40::q_delay_func, this));
		}

		SC_METHOD(function);
		sensitive << A << B << C << D;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}

	const unit::energy_t xor20::q_pos_energy[9]
	{	
		0.1744643549822773_pJ,
		0.1742623810183882_pJ,
		0.1741127678764639_pJ,
		0.1740122028854428_pJ,
		0.1739460977162783_pJ,
		0.1739183420622563_pJ,
		0.1739421775170957_pJ,
		0.1739854522536713_pJ,
		0.1740443099326681_pJ
	};
	
	const unit::energy_t xor20::q_neg_energy[9]
	{
		0.2089818577008407_pJ,
		0.2091558569849892_pJ,
		0.2093773189145205_pJ,
		0.2096364406943405_pJ,
		0.2099350397587013_pJ,
		0.210243454477245_pJ,
		0.2105922315214419_pJ,
		0.2109957719315626_pJ,
		0.2114099620729207_pJ
	};
	
	const unit::current_t xor20::leakage_current[9]
	{
		9.923384757785116_pA,
		9.95807695306064_pA,
		10.03600738214377_pA,
		10.20212156911714_pA,
		10.53995706243662_pA,
		11.19862308028297_pA,
		12.43480869016647_pA,
		14.67627821165826_pA,
		18.6153803764585_pA
	};
	
	const unit::time_t xor20::q_pos_delay[9]
	{
		0.3236032265129_ns,
		0.3274818592939_ns,
		0.3312685196429_ns,
		0.3349658585960_ns,
		0.3385601693529_ns,
		0.3420893435310_ns,
		0.3455284437149_ns,
		0.3488841917929_ns,
		0.3521555662399_ns
	};
	
	const unit::time_t xor20::q_neg_delay[9]
	{
		0.3886852585801_ns,
		0.3956580096500_ns,
		0.4026107529799_ns,
		0.4095439155700_ns,
		0.4164470504398_ns,
		0.4233285658199_ns,
		0.4301908207799_ns,
		0.4370228114098_ns,
		0.4438257462899_ns
	};
	
	void xor20::function()
	{
		q_delay.write(A.read() ^ B.read());
	}

	sc_time xor20::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t xor20::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t xor20::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t xor20::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}

	xor20::xor20(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&xor20::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&xor20::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(10.5_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(xor20);

		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&xor20::q_delay_func, this));
		}

		SC_METHOD(function);
		sensitive << A << B;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t xor30::q_pos_energy[9]
	{	
		0.2693172083055327_pJ,
		0.2696665751011353_pJ,
		0.2700761486776799_pJ,
		0.2705566267833781_pJ,
		0.2710737273464807_pJ,
		0.2716417684526587_pJ,
		0.2722571602126312_pJ,
		0.2729182530632236_pJ,
		0.2735981689449942_pJ
	};
	
	const unit::energy_t xor30::q_neg_energy[9]
	{
		0.4518165708852703_pJ,
		0.4487639622353273_pJ,
		0.4460697571340269_pJ,
		0.4437053601078663_pJ,
		0.4415961051770321_pJ,
		0.4397149619012226_pJ,
		0.4380967371690384_pJ,
		0.4366922501926011_pJ,
		0.4356066283616902_pJ
	};
	
	const unit::current_t xor30::leakage_current[9]
	{
		16.863821337307_pA,
		16.96501235098934_pA,
		17.14554013388486_pA,
		17.48907884184099_pA,
		18.15149475936142_pA,
		19.40957400808583_pA,
		21.73771765471419_pA,
		25.91127255795434_pA,
		33.16084833196903_pA
	};
	
	const unit::time_t xor30::q_pos_delay[9]
	{
		0.5888867816799_ns,
		0.6007469563199_ns,
		0.6124937449000_ns,
		0.6241644132401_ns,
		0.6357222232102_ns,
		0.6471604845899_ns,
		0.6585097330901_ns,
		0.6697251476300_ns,
		0.6808244489700_ns
	};
	
	const unit::time_t xor30::q_neg_delay[9]
	{
		0.4566675128703_ns,
		0.4651471963203_ns,
		0.4735975846498_ns,
		0.4820106740803_ns,
		0.4903916865098_ns,
		0.4987252086900_ns,
		0.5070111171903_ns,
		0.5152550667502_ns,
		0.5234679160296_ns
	};
	
	void xor30::function()
	{
		q_delay.write(A.read() ^ B.read() ^ C.read());
	}

	sc_time xor30::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t xor30::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t xor30::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t xor30::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
	
	xor30::xor30(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&xor30::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&xor30::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(14_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(xor30);

		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&xor30::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << C;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t xnr20::q_pos_energy[9]
	{	
		0.1726317987962208_pJ,
		0.1725370390079731_pJ,
		0.1724683786464868_pJ,
		0.1724334138598948_pJ,
		0.1724422354652371_pJ,
		0.172475472932059_pJ,
		0.1725332051715432_pJ,
		0.172633859019946_pJ,
		0.1727268906712199_pJ
	};
	
	const unit::energy_t xnr20::q_neg_energy[9]
	{
		0.1996279225015912_pJ,
		0.2001837386062392_pJ,
		0.2007629878323459_pJ,
		0.2013647591098147_pJ,
		0.2019683156019004_pJ,
		0.2025960094908263_pJ,
		0.2032349961061697_pJ,
		0.2038989465618795_pJ,
		0.2045691306901831_pJ
	};
	
	const unit::current_t xnr20::leakage_current[9]
	{
		6.950137035347556_pA,
		7.031739013366155_pA,
		7.166105303375561_pA,
		7.408765731472227_pA,
		7.860445367555714_pA,
		8.695790548932864_pA,
		10.020651943785032_pA,
		10.285149436454421_pA,
		10.732879970152854_pA
	};
	
	const unit::time_t xnr20::q_pos_delay[9]
	{
		0.3982980226001_ns,
		0.4021227747100_ns,
		0.4058013096799_ns,
		0.4093838242199_ns,
		0.4128381152399_ns,
		0.4161484295100_ns,
		0.4193655578899_ns,
		0.4224711381899_ns,
		0.4254742211498_ns
	};
	
	const unit::time_t xnr20::q_neg_delay[9]
	{
		0.4893334601900_ns,
		0.4970064150997_ns,
		0.5047172139295_ns,
		0.5124093397295_ns,
		0.5200254563097_ns,
		0.5275978643000_ns,
		0.5350855901498_ns,
		0.5425072035094_ns,
		0.5498666037300_ns
	};
	
	void xnr20::function()
	{
		q_delay.write(~(A.read() ^ B.read()));
	}

	sc_time xnr20::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t xnr20::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t xnr20::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t xnr20::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}

	xnr20::xnr20(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&xnr20::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&xnr20::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(xnr20);

		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&xnr20::q_delay_func, this));
		}

		SC_METHOD(function);
		sensitive << A << B;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t inv0::q_pos_energy[9]
	{	
		0.1054251841961055_pJ,
		0.1051578153948485_pJ,
		0.1049223372290877_pJ,
		0.1047209950430621_pJ,
		0.1045499846591894_pJ,
		0.1044063463482446_pJ,
		0.1042869049508217_pJ,
		0.1041907484112528_pJ,
		0.104137471403211_pJ
	};
	
	const unit::energy_t inv0::q_neg_energy[9]
	{
		86.79007346143579_fJ,
		86.48620234972735_fJ,
		86.17721151319098_fJ,
		85.89251109724345_fJ,
		85.61997003981277_fJ,
		85.34485541292719_fJ,
		85.12678313524349_fJ,
		84.91993572977138_fJ,
		84.73736026972145_fJ
	};
	
	const unit::current_t inv0::leakage_current[9]
	{
		3.311685994877495_pA,
		3.329014642828491_pA,
		3.367134225376097_pA,
		3.446980352008734_pA,
		3.606085483954583_pA,
		3.909118219712851_pA,
		4.463072218783476_pA,
		5.438588795139405_pA,
		7.098946134355409_pA
	};
	
	const unit::time_t inv0::q_pos_delay[9]
	{
		0.3349130261169_ns,
		0.3376808180710_ns,
		0.3403325524690_ns,
		0.3428735440671_ns,
		0.3453086101920_ns,
		0.3476378388281_ns,
		0.3498638250250_ns,
		0.3519917108830_ns,
		0.3540244202910_ns
	};
	
	const unit::time_t inv0::q_neg_delay[9]
	{
		0.1480891395399_ns,
		0.1533673534901_ns,
		0.1584242007398_ns,
		0.1634607754401_ns,
		0.1683837587899_ns,
		0.1733719190499_ns,
		0.1780244183499_ns,
		0.1830690653199_ns,
		0.1877843504300_ns
	};
	
	void inv0::function()
	{
		q_delay.write(~A.read());
	}
	
	sc_time inv0::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t inv0::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t inv0::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t inv0::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
			
	inv0::inv0(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&inv0::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&inv0::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(3.5_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(inv0);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&inv0::q_delay_func, this));
		}		
		
		SC_METHOD(function);
		sensitive << A;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t oai210::q_pos_energy[9]
	{	
		0.190768740654577_pJ,
		0.1907008032634411_pJ,
		0.190690095620847_pJ,
		0.1907251099492205_pJ,
		0.1908085097306261_pJ,
		0.1909157459848085_pJ,
		0.1910659848827644_pJ,
		0.1912422082248103_pJ,
		0.1914478753838815_pJ
	};
	
	const unit::energy_t oai210::q_neg_energy[9]
	{
		0.1235251499902165_pJ,
		0.1226875923781016_pJ,
		0.121794330930122_pJ,
		0.1209793292996238_pJ,
		0.1201945064976081_pJ,
		0.119466038559929_pJ,
		0.1187353139987936_pJ,
		0.118120246628308_pJ,
		0.1175537647941196_pJ
	};
	
	const unit::current_t oai210::leakage_current[9]
	{
		2.205465844672998_pA,
		2.213816448702334_pA,
		2.232710298715573_pA,
		2.272806861024725_pA,
		2.352452533673073_pA,
		2.499636627099975_pA,
		2.749738157141829_pA,
		3.132868663135539_pA,
		4.311062063906497_pA
	};
	
	const unit::time_t oai210::q_pos_delay[9]
	{
		0.3687948995199_ns,
		0.3732953613401_ns,
		0.3777035678701_ns,
		0.3819793872200_ns,
		0.3861390881702_ns,
		0.3901922428001_ns,
		0.3941557660698_ns,
		0.3979775399299_ns,
		0.4016946223300_ns
	};
	
	const unit::time_t oai210::q_neg_delay[9]
	{
		0.1673359980710_ns,
		0.1731280580509_ns,
		0.1788097207904_ns,
		0.1845585913710_ns,
		0.1902180764713_ns,
		0.1958521476406_ns,
		0.2013104577505_ns,
		0.2069616084709_ns,
		0.2124034178403_ns
	};
	
	void oai210::function()
	{
		q_delay.write(~((A.read() | B.read()) & C.read()));
	}
	
	sc_time oai210::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t oai210::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t oai210::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t oai210::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
			
	oai210::oai210(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&oai210::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&oai210::q_neg_dissipation, this)),
		layout_item(id, layer_name, position,  layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(oai210);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&oai210::q_delay_func, this));
		}
		
		
		SC_METHOD(function);
		sensitive << A << B << C;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t oai2110::q_pos_energy[9]
	{	
		0.1762197998114549_pJ,
		0.1759732685301912_pJ,
		0.1757785743148725_pJ,
		0.1756373624999636_pJ,
		0.1755487794021286_pJ,
		0.1754902016875034_pJ,
		0.1754565327136942_pJ,
		0.1754653402214019_pJ,
		0.1754965207701192_pJ
	};
	
	const unit::energy_t oai2110::q_neg_energy[9]
	{
		0.1432974329261294_pJ,
		0.1430078973493782_pJ,
		0.1428227028429928_pJ,
		0.1427259634610894_pJ,
		0.1427223032569691_pJ,
		0.1427535421694066_pJ,
		0.1429042153584836_pJ,
		0.1431209977127335_pJ,
		0.1433656287932099_pJ
	};
	
	const unit::current_t oai2110::leakage_current[9]
	{
		1.323924043309695_pA,
		1.330008173019744_pA,
		1.343875724983152_pA,
		1.373517166444295_pA,
		1.432741630560137_pA,
		1.542632579227829_pA,
		1.72976715898272_pA,
		2.016454815257566_pA,
		2.685290947488003_pA
	};
	
	const unit::time_t oai2110::q_pos_delay[9]
	{
		0.3590842918400_ns,
		0.3635652332399_ns,
		0.3679900560600_ns,
		0.3722596597301_ns,
		0.3765003665200_ns,
		0.3806181649000_ns,
		0.3846501193201_ns,
		0.3885608639101_ns,
		0.3923840483299_ns
	};
	
	const unit::time_t oai2110::q_neg_delay[9]
	{
		0.2355402570696_ns,
		0.2429069727987_ns,
		0.2502437273292_ns,
		0.2575159962081_ns,
		0.2647145983995_ns,
		0.2718812245787_ns,
		0.2789677198893_ns,
		0.2860269502784_ns,
		0.2930241252888_ns
	};
	
	void oai2110::function()
	{
		q_delay.write(~((A.read() | B.read()) & C.read() & D.read()));
	}
	
	sc_time oai2110::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t oai2110::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t oai2110::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t oai2110::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
			
	oai2110::oai2110(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&oai2110::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&oai2110::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(oai2110);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&oai2110::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << C << D;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t oai220::q_pos_energy[9]
	{	
		0.1539577956400924_pJ,
		0.1536646570161436_pJ,
		0.1534171351260358_pJ,
		0.1532090059259204_pJ,
		0.1530382174874683_pJ,
		0.1529086882601623_pJ,
		0.1527936864827302_pJ,
		0.1527111779998734_pJ,
		0.1526453107961294_pJ
	};
	
	const unit::energy_t oai220::q_neg_energy[9]
	{
		0.1568347963988346_pJ,
		0.1558935799540812_pJ,
		0.155008867924955_pJ,
		0.1541628043148135_pJ,
		0.1533706168847006_pJ,
		0.1527485345719676_pJ,
		0.1520278997364686_pJ,
		0.1513560712654545_pJ,
		0.1507111833378683_pJ
	};
	
	const unit::current_t oai220::leakage_current[9]
	{
		3.30953320304158_pA,
		3.324016195316364_pA,
		3.356667246523431_pA,
		3.425870612102754_pA,
		3.563643238267255_pA,
		3.820322296817896_pA,
		4.264175261417717_pA,
		4.967859158609346_pA,
		5.972251966688776_pA
	};
	
	const unit::time_t oai220::q_pos_delay[9]
	{
		0.3681368584201_ns,
		0.3715355662500_ns,
		0.3748205306401_ns,
		0.3779848369399_ns,
		0.3810449691199_ns,
		0.3840207318300_ns,
		0.3868727210601_ns,
		0.3896476218701_ns,
		0.3923647691901_ns
	};
	
	const unit::time_t oai220::q_neg_delay[9]
	{
		0.04675943996956_ns,
		0.05104203480996_ns,
		0.05539762772986_ns,
		0.05952112877974_ns,
		0.06381569123006_ns,
		0.06789106414951_ns,
		0.07197931613970_ns,
		0.07607139454956_ns,
		0.08019456545987_ns
	};
	
	void oai220::function()
	{
		q_delay.write(~((A.read() | B.read()) & (C.read() | D.read())));
	}
	
	sc_time oai220::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t oai220::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t oai220::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t oai220::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
			
	oai220::oai220(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&oai220::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&oai220::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(oai220);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&oai220::q_delay_func, this));
		}		
		
		SC_METHOD(function);
		sensitive << A << B << C << D;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t oai310::q_pos_energy[9]
	{	
		0.241419589820581_pJ,
		0.2417768577606795_pJ,
		0.2421825905825084_pJ,
		0.2426426832028697_pJ,
		0.2431296483163643_pJ,
		0.2436579018616273_pJ,
		0.244234701656517_pJ,
		0.2448108997715793_pJ,
		0.2454298917576307_pJ
	};
	
	const unit::energy_t oai310::q_neg_energy[9]
	{
		0.1437831652324744_pJ,
		0.1427495478289242_pJ,
		0.1415517451738192_pJ,
		0.140429370089447_pJ,
		0.1392913970687328_pJ,
		0.138111571818919_pJ,
		0.1373184055834208_pJ,
		0.1362359453484572_pJ,
		0.13553477767171_pJ
	};
	
	const unit::current_t oai310::leakage_current[9]
	{
		2.480668448434642_pA,
		2.489343727576598_pA,
		2.509025243797385_pA,
		2.550838301120127_pA,
		2.633832223614051_pA,
		2.786595240877915_pA,
		3.042797087533686_pA,
		3.718514431853576_pA,
		5.205427712114572_pA
	};
	
	const unit::time_t oai310::q_pos_delay[9]
	{
		0.3917124630700_ns,
		0.3984454258998_ns,
		0.4049847402800_ns,
		0.4114593048003_ns,
		0.4178164707799_ns,
		0.4240471475901_ns,
		0.4301377659499_ns,
		0.4360910947501_ns,
		0.4419359724000_ns
	};
	
	const unit::time_t oai310::q_neg_delay[9]
	{
		0.1761423935481_ns,
		0.1822620164196_ns,
		0.1883614144279_ns,
		0.1943822292882_ns,
		0.2004275996980_ns,
		0.2063260799090_ns,
		0.2122412674281_ns,
		0.2181250907283_ns,
		0.2239583872189_ns
	};
	
	void oai310::function()
	{
		q_delay.write(~((A.read() | B.read() | C.read()) & D.read()));
	}
	
	sc_time oai310::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t oai310::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t oai310::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t oai310::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
			
	oai310::oai310(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&oai310::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&oai310::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))	
	{
		SC_HAS_PROCESS(oai310);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&oai310::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << C << D;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t aoi210::q_pos_energy[9]
	{	
		0.1542679082024457_pJ,
		0.1540027088673391_pJ,
		0.1537939980995955_pJ,
		0.1536189153311749_pJ,
		0.1534924924490982_pJ,
		0.153404590174592_pJ,
		0.1533560395153029_pJ,
		0.1533159451106364_pJ,
		0.1533196831852259_pJ
	};
	
	const unit::energy_t aoi210::q_neg_energy[9]
	{
		0.11912868968393_pJ,
		0.1184157931367997_pJ,
		0.1177482790314997_pJ,
		0.117101212516619_pJ,
		0.116485760437695_pJ,
		0.1159163563555483_pJ,
		0.1153646907711734_pJ,
		0.1148421272114204_pJ,
		0.1143352538374991_pJ
	};
	
	const unit::current_t aoi210::leakage_current[9]
	{
		4.96648035197452_pA,
		4.991048520237931_pA,
		5.045493667142879_pA,
		5.159941557461472_pA,
		5.387933159343626_pA,
		5.819305704117668_pA,
		6.595186635498932_pA,
		7.922545796936619_pA,
		10.008510031116699_pA
	};
	
	const unit::time_t aoi210::q_pos_delay[9]
	{
		0.3223692180999_ns,
		0.3262129464098_ns,
		0.3299762507498_ns,
		0.3336436129198_ns,
		0.3372217110601_ns,
		0.3406939261898_ns,
		0.3441311035501_ns,
		0.3474839107199_ns,
		0.3507252663100_ns
	};
	
	const unit::time_t aoi210::q_neg_delay[9]
	{
		0.08509735934001_ns,
		0.08962640372011_ns,
		0.09404839383963_ns,
		0.09864139055996_ns,
		0.1029363979105_ns,
		0.1073067258702_ns,
		0.1117864173700_ns,
		0.1159777261800_ns,
		0.1201446931402_ns
	};
	
	void aoi210::function()
	{
		q_delay.write(~((A.read() & B.read()) | C.read()));
	}
	
	sc_time aoi210::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t aoi210::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t aoi210::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t aoi210::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
			
	aoi210::aoi210(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&aoi210::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&aoi210::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(aoi210);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&aoi210::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << C;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t aoi2110::q_pos_energy[9]
	{	
		0.2047516409722617_pJ,
		0.2048861853969179_pJ,
		0.205061574233061_pJ,
		0.2052853760629078_pJ,
		0.2055496039555364_pJ,
		0.2058509424400663_pJ,
		0.2061906284339212_pJ,
		0.2065509740673468_pJ,
		0.2069116093014821_pJ
	};
	
	const unit::energy_t aoi2110::q_neg_energy[9]
	{
		0.1597617866967223_pJ,
		0.1589084742486881_pJ,
		0.1580758891425773_pJ,
		0.157321186205449_pJ,
		0.1565694557697276_pJ,
		0.155848099134944_pJ,
		0.1551705538567843_pJ,
		0.1545228149939615_pJ,
		0.1538785057215703_pJ
	};
	
	const unit::current_t aoi2110::leakage_current[9]
	{
		8.278407549557784_pA,
		8.320080274867584_pA,
		8.412645001283589_pA,
		8.606939013582812_pA,
		8.994035739475953_pA,
		9.728441005489683_pA,
		11.058275908114_pA,
		13.36115159052258_pA,
		17.18406332950591_pA
	};
	
	const unit::time_t aoi2110::q_pos_delay[9]
	{
		0.3497941555904_ns,
		0.3558297767798_ns,
		0.3617566791999_ns,
		0.3676096497409_ns,
		0.3733336838205_ns,
		0.3789363384098_ns,
		0.3844652178796_ns,
		0.3898801279398_ns,
		0.3951477151003_ns
	};
	
	const unit::time_t aoi2110::q_neg_delay[9]
	{
		0.05151441508880_ns,
		0.05581182471883_ns,
		0.05999721216805_ns,
		0.06414149572905_ns,
		0.06823044717957_ns,
		0.07215138068847_ns,
		0.07614150266912_ns,
		0.08015434242918_ns,
		0.08398175147961_ns
	};
	
	void aoi2110::function()
	{
		q_delay.write(~((A.read() & B.read()) | C.read() | D.read()));
	}
	
	sc_time aoi2110::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t aoi2110::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t aoi2110::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t aoi2110::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
			
	aoi2110::aoi2110(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&aoi2110::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&aoi2110::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(aoi2110);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&aoi2110::q_delay_func, this));
		}		
		
		SC_METHOD(function);
		sensitive << A << B << C << D;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t aoi220::q_pos_energy[9]
	{	
		0.1581189008308648_pJ,
		0.1579112953265916_pJ,
		0.157753309191459_pJ,
		0.1576420447403245_pJ,
		0.1575664979216796_pJ,
		0.1575241293619864_pJ,
		0.1575227264742986_pJ,
		0.1575390385609056_pJ,
		0.1575733100931355_pJ
	};
	
	const unit::energy_t aoi220::q_neg_energy[9]
	{
		0.2078320076663097_pJ,
		0.2075866622543569_pJ,
		0.2073708742265876_pJ,
		0.2072138428602179_pJ,
		0.207084977586819_pJ,
		0.2069979870945496_pJ,
		0.2069490870590554_pJ,
		0.2069357194170863_pJ,
		0.2069508383086657_pJ,
	};
	
	const unit::current_t aoi220::leakage_current[9]
	{
		3.309536672488532_pA,
		3.324010529585659_pA,
		3.356661659193885_pA,
		3.425865187857895_pA,
		3.563638129812181_pA,
		3.820317751412772_pA,
		4.264171622249929_pA,
		4.967856802934945_pA,
		5.972251169557615_pA
	};
	
	const unit::time_t aoi220::q_pos_delay[9]
	{
		0.3550510317897_ns,
		0.3598568255997_ns,
		0.3645634184698_ns,
		0.3691773474494_ns,
		0.3736970515794_ns,
		0.3781226985498_ns,
		0.3824440845300_ns,
		0.3866555608201_ns,
		0.3907565954997_ns
	};
	
	const unit::time_t aoi220::q_neg_delay[9]
	{
		0.08295528191068_ns,
		0.08783193147008_ns,
		0.09268702500066_ns,
		0.09730580883015_ns,
		0.1020852990306_ns,
		0.1065282245410_ns,
		0.1111809621707_ns,
		0.1157396021411_ns,
		0.1202521845909_ns
	};
	
	void aoi220::function()
	{
		q_delay.write(~((A.read() & B.read()) | (C.read()) & D.read()));
	}
	
	sc_time aoi220::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t aoi220::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t aoi220::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t aoi220::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
			
	aoi220::aoi220(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&aoi220::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&aoi220::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(aoi220);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&aoi220::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << C << D;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t aoi310::q_pos_energy[9]
	{	
		0.2494654522364969_pJ,
		0.2497426569221312_pJ,
		0.2500785724373837_pJ,
		0.2504536589589655_pJ,
		0.2508661199559951_pJ,
		0.2513107533581422_pJ,
		0.2517818593998181_pJ,
		0.2522711294823254_pJ,
		0.2527737435275377_pJ
	};
	
	const unit::energy_t aoi310::q_neg_energy[9]
	{
		0.1267752831002518_pJ,
		0.1258332516665713_pJ,
		0.1249678347297219_pJ,
		0.1241312740208778_pJ,
		0.123367791820508_pJ,
		0.1225515695220281_pJ,
		0.1219588691542895_pJ,
		0.1213572502533299_pJ,
		0.120755436369504_pJ
	};
	
	const unit::current_t aoi310::leakage_current[9]
	{
		4.414756754610006_pA,
		4.436859981723295_pA,
		4.485877495898057_pA,
		4.58902571687458_pA,
		4.794624844719078_pA,
		5.183554692167708_pA,
		5.882370813078002_pA,
		7.099588069267089_pA,
		9.338324074786448_pA
	};
	
	const unit::time_t aoi310::q_pos_delay[9]
	{
		0.4569135052197_ns,
		0.4634965107896_ns,
		0.4699648771604_ns,
		0.4762483937192_ns,
		0.4824110197190_ns,
		0.4884216323199_ns,
		0.4943074203901_ns,
		0.5000232348797_ns,
		0.5055909853801_ns
	};
	
	const unit::time_t aoi310::q_neg_delay[9]
	{
		0.08487668947973_ns,
		0.08932588849810_ns,
		0.09372307912770_ns,
		0.09806279362854_ns,
		0.1024821436785_ns,
		0.1065926058983_ns,
		0.1108578355879_ns,
		0.1151312461098_ns,
		0.1192923140285_ns
	};
	
	void aoi310::function()
	{
		q_delay.write(~((A.read() & B.read() & C.read()) | D.read()));
	}
	
	sc_time aoi310::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t aoi310::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t aoi310::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t aoi310::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
			
	aoi310::aoi310(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&aoi310::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&aoi310::q_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(aoi310);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&aoi310::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << C << D;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
	}
	
	const unit::energy_t df3::q_pos_energy[9]
	{	
		0.266098008398018_pJ,
		0.268125543184776_pJ,
		0.270212621103403_pJ,
		0.272354184762555_pJ,
		0.274532459207319_pJ,
		0.276738247121387_pJ,
		0.279009058737062_pJ,
		0.281332977266854_pJ,
		0.283698036041688_pJ
	};
	
	const unit::energy_t df3::q_neg_energy[9]
	{
		0.28068357431604_pJ,
		0.282771691229646_pJ,
		0.284891944583388_pJ,
		0.287058403710403_pJ,
		0.289269517280915_pJ,
		0.291534298781891_pJ,
		0.293842866352952_pJ,
		0.296196632843958_pJ,
		0.298599717003597_pJ
	};
	
	const unit::energy_t df3::qn_pos_energy[9]
	{	
		0.266098008398018_pJ,
		0.268125543184776_pJ,
		0.270212621103403_pJ,
		0.272354184762555_pJ,
		0.274532459207319_pJ,
		0.276738247121387_pJ,
		0.279009058737062_pJ,
		0.281332977266854_pJ,
		0.283698036041688_pJ
	};
	
	const unit::energy_t df3::qn_neg_energy[9]
	{
		0.28068357431604_pJ,
		0.282771691229646_pJ,
		0.284891944583388_pJ,
		0.287058403710403_pJ,
		0.289269517280915_pJ,
		0.291534298781891_pJ,
		0.293842866352952_pJ,
		0.296196632843958_pJ,
		0.298599717003597_pJ
	};
	
	const unit::energy_t df3::d_pos_energy[9]
	{	
		0.2058786119760282_pJ,
		0.2060555657278108_pJ,
		0.2063320260353181_pJ,
		0.2066942548728526_pJ,
		0.2071730659412357_pJ,
		0.2077513605223518_pJ,
		0.2084380544389807_pJ,
		0.2091521580445179_pJ,
		0.2099251331985138_pJ
	};
	
	const unit::energy_t df3::d_neg_energy[9]
	{
		0.2397763451658011_pJ,
		0.2404767949060894_pJ,
		0.2412385715724387_pJ,
		0.2420571148922824_pJ,
		0.2429389718133023_pJ,
		0.2438674609365809_pJ,
		0.2448353734429588_pJ,
		0.2458505518042288_pJ,
		0.2468977670424804_pJ
	};
	
	const unit::energy_t df3::c_pos_energy[9]
	{	
		0.1621668830182304_pJ,
		0.1621878706176796_pJ,
		0.1622338979346841_pJ,
		0.1623295847264256_pJ,
		0.1624639907625991_pJ,
		0.1626326636777242_pJ,
		0.1628382911090744_pJ,
		0.1630780548511412_pJ,
		0.1633477975630484_pJ
	};
	
	const unit::energy_t df3::c_neg_energy[9]
	{
		0.1621668830182304_pJ,
		0.1621878706176796_pJ,
		0.1622338979346841_pJ,
		0.1623295847264256_pJ,
		0.1624639907625991_pJ,
		0.1626326636777242_pJ,
		0.1628382911090744_pJ,
		0.1630780548511412_pJ,
		0.1633477975630484_pJ
	};
	
	const unit::current_t df3::leakage_current[9]
	{
		29.78221461418156_pA,
		29.90974292081828_pA,
		30.20406604571394_pA,
		30.84786609282688_pA,
		32.18991760711409_pA,
		34.86875366855098_pA,
		40.0101769526828_pA,
		49.53289242479016_pA,
		66.60853542989383_pA
	};
	
	const unit::time_t df3::q_pos_delay[9]
	{
		0.4736505188595_ns,
		0.4880610251298_ns,
		0.5024480665503_ns,
		0.5168042142500_ns,
		0.5311423368505_ns,
		0.5454246333603_ns,
		0.5596846564305_ns,
		0.5738706451595_ns,
		0.5879824214602_ns
	};
	
	const unit::time_t df3::q_neg_delay[9]
	{
		0.4977691715505_ns,
		0.5128425598990_ns,
		0.5279037309898_ns,
		0.5428709168786_ns,
		0.5577635154583_ns,
		0.5725720883799_ns,
		0.5872886293202_ns,
		0.6019386453092_ns,
		0.6164762917898_ns	
	};
	
	const unit::time_t df3::qn_pos_delay[9]
	{
		0.5930999678301_ns,
		0.6113196546401_ns,
		0.6295491574507_ns,
		0.6477236192404_ns,
		0.6659201620602_ns,
		0.6840312680106_ns,
		0.7021389306399_ns,
		0.7201414034197_ns,
		0.7380882382500_ns
	};
	
	const unit::time_t df3::qn_neg_delay[9]
	{
		0.6212043226500_ns,
		0.6398850669488_ns,
		0.6585146223599_ns,
		0.6770639166406_ns,
		0.6955344168282_ns,
		0.7139125950105_ns,
		0.7322045389791_ns,
		0.7503918612887_ns,
		0.7684874114884_ns
	};
	
	void df3::function()
	{
		q_delay.write(D.read());
		qn_delay.write(~(D.read()));
	}
	
	sc_time df3::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	sc_time df3::qn_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, qn_pos_delay), interpolate(temperature, qn_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t df3::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t df3::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t df3::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
	
	unit::energy_t df3::qn_pos_dissipation()
	{
		return interpolate(temperature, qn_pos_energy);
	}
	
	unit::energy_t df3::qn_neg_dissipation()
	{
		return interpolate(temperature, qn_neg_energy);
	}
	
	unit::energy_t df3::d_pos_dissipation()
	{
		return interpolate(temperature, d_pos_energy);
	}
	
	unit::energy_t df3::d_neg_dissipation()
	{
		return interpolate(temperature, d_neg_energy);
	}
	
	unit::energy_t df3::c_pos_dissipation()
	{
		return interpolate(temperature, c_pos_energy);
	}
	
	unit::energy_t df3::c_neg_dissipation()
	{
		return interpolate(temperature, c_neg_energy);
	}
	
	df3::df3(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&df3::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&df3::q_neg_dissipation, this)),
		qn_pos("qn_pos",std::bind(&df3::qn_pos_dissipation, this)),
		qn_neg("qn_neg",std::bind(&df3::qn_neg_dissipation, this)),
		d_pos("d_pos",std::bind(&df3::d_pos_dissipation, this)),
		d_neg("d_neg",std::bind(&df3::d_neg_dissipation, this)),
		c_pos("c_pos",std::bind(&df3::c_pos_dissipation, this)),
		c_neg("c_neg",std::bind(&df3::c_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(21_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(df3);
		
		q_delay(Q);
		qn_delay(QN);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&df3::q_delay_func, this));
			qn_delay.set_delay_function(std::bind(&df3::qn_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << C.pos(); // << rst;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
		
		SC_ACTIVITY(qn_pos);
		sensitive << QN.pos();
		dont_initialize();
		
		SC_ACTIVITY(qn_neg);
		sensitive << QN.neg();
		dont_initialize();
		
		SC_ACTIVITY(d_pos);
		sensitive << D.pos();
		dont_initialize();
		
		SC_ACTIVITY(d_neg);
		sensitive << D.neg();
		dont_initialize();
		
		SC_ACTIVITY(c_pos);
		sensitive << C.pos();
		dont_initialize();
		
		SC_ACTIVITY(c_neg);
		sensitive << C.neg();
		dont_initialize();
	}
	
	const unit::energy_t dfe1::q_pos_energy[9]
	{	
		0.18832669610053_pJ,
		0.189295997298971_pJ,
		0.190288420674442_pJ,
		0.191259370862551_pJ,
		0.192227559963759_pJ,
		0.193188324626369_pJ,
		0.194171095176251_pJ,
		0.195178922247036_pJ,
		0.196217439624789_pJ
	};
	
	const unit::energy_t dfe1::q_neg_energy[9]
	{
		0.18798651862179_pJ,
		0.188867489393609_pJ,
		0.189747234713193_pJ,
		0.190643909353966_pJ,
		0.191564535967228_pJ,
		0.192505474133388_pJ,
		0.193458923025477_pJ,
		0.194420161022684_pJ,
		0.195399509607192_pJ
	};
	
	const unit::energy_t dfe1::qn_pos_energy[9]
	{	
		0.18832669610053_pJ,
		0.189295997298972_pJ,
		0.190288420674442_pJ,
		0.191259370862551_pJ,
		0.192227559963759_pJ,
		0.193188324626369_pJ,
		0.194171095176251_pJ,
		0.195178922247037_pJ,
		0.196217439624789_pJ,
	};
	
	const unit::energy_t dfe1::qn_neg_energy[9]
	{
		0.18798651862179_pJ,
		0.188867489393609_pJ,
		0.189747234713193_pJ,
		0.190643909353966_pJ,
		0.191564535967228_pJ,
		0.192505474133388_pJ,
		0.193458923025477_pJ,
		0.194420161022684_pJ,
		0.195399509607192_pJ
	};
	
	const unit::energy_t dfe1::sample_pos_energy[9]
	{	
		0.3969429455195172_pJ,
		0.398345690360827_pJ,
		0.3998625060103989_pJ,
		0.4014512851442713_pJ,
		0.4031010929523228_pJ,
		0.4048659608536849_pJ,
		0.4066818123288218_pJ,
		0.4085531032608383_pJ,
		0.4104562578691336_pJ
	};
	
	const unit::energy_t dfe1::sample_neg_energy[9]
	{
		0.3710210255235357_pJ,
		0.3732477474407894_pJ,
		0.3755505061800583_pJ,
		0.3778811191693959_pJ,
		0.380229763481059_pJ,
		0.3826518044128194_pJ,
		0.3850647033891385_pJ,
		0.38753845384358_pJ,
		0.3900490131734858_pJ
	};
	
	const unit::energy_t dfe1::e_pos_energy[9]
	{	
		86.59917080997178_fJ,
		86.74466251780748_fJ,
		87.013291195011_fJ,
		87.67789531997612_fJ,
		88.92670989727663_fJ,
		91.23213763588025_fJ,
		95.36063062931187_fJ,
		102.1324338955183_fJ,
		110.0328491158808_fJ
	};
	
	const unit::energy_t dfe1::e_neg_energy[9]
	{
		80.32839824307954_fJ,
		80.26431945229873_fJ,
		80.22338067646305_fJ,
		80.20285313807247_fJ,
		80.20306918705632_fJ,
		80.23499956034883_fJ,
		80.26902829975346_fJ,
		80.31794790908044_fJ,
		80.38801113421485_fJ
	};
	
	const unit::energy_t dfe1::c_pos_energy[9]
	{	
		0.1130807349193212_pJ,
		0.1128532699385397_pJ,
		0.112671526031783_pJ,
		0.1125904553309405_pJ,
		0.1125822708332823_pJ,
		0.1126179140885771_pJ,
		0.1126760424446175_pJ,
		0.1127531168959647_pJ,
		0.1128375439177044_pJ
	};
	
	const unit::energy_t dfe1::c_neg_energy[9]
	{
		0.1384542129012558_pJ,
		0.1383846859425308_pJ,
		0.1383795070476808_pJ,
		0.1383965841765252_pJ,
		0.1384509322912205_pJ,
		0.1385392089818451_pJ,
		0.1386637092769773_pJ,
		0.1388202950647375_pJ,
		0.1390056263801795_pJ
	};
	
	const unit::current_t dfe1::leakage_current[9]
	{
		33.07330682006955_pA,
		33.18562089206663_pA,
		33.44335232260589_pA,
		34.00461643121025_pA,
		35.17087549285014_pA,
		37.49409634260041_pA,
		41.94885263670751_pA,
		50.20054753256591_pA,
		65.01266853336778_pA
	};
	
	const unit::time_t dfe1::q_pos_delay[9]
	{
		0.4830317690197_ns,
		0.4973634255791_ns,
		0.5116347663292_ns,
		0.5258540725994_ns,
		0.5399860656394_ns,
		0.5540686098699_ns,
		0.5680404231799_ns,
		0.5819582983500_ns,
		0.5957749524595_ns
	};
	
	const unit::time_t dfe1::q_neg_delay[9]
	{
		0.4692919616199_ns,
		0.4838402915099_ns,
		0.4983513799098_ns,
		0.5128135666795_ns,
		0.5272075284401_ns,
		0.5415486443500_ns,
		0.5557858791749_ns,
		0.5700231139995_ns,
		0.5841181403096_ns
	};
	
	const unit::time_t dfe1::qn_pos_delay[9]
	{
		0.5390766934601_ns,
		0.5554414366896_ns,
		0.5717676808995_ns,
		0.5880646168292_ns,
		0.6043311296403_ns,
		0.6205425894399_ns,
		0.6366862664197_ns,
		0.6527955227902_ns,
		0.6688324910295_ns
	};
	
	const unit::time_t dfe1::qn_neg_delay[9]
	{
		0.5462383302599_ns,
		0.5628901266101_ns,
		0.5794843878299_ns,
		0.5960237753698_ns,
		0.6124590680596_ns,
		0.6288121320698_ns,
		0.6450150617948_ns,
		0.6612179915199_ns,
		0.6772322701398_ns
	};
	
	void dfe1::function()
	{
		q_delay.write( (E.read() == SC_LOGIC_1 ? D.read() : Q.read()));
		qn_delay.write( ~(E.read() == SC_LOGIC_1 ? D.read() : Q.read()));
	}
	
	sc_time dfe1::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	sc_time dfe1::qn_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, qn_pos_delay), interpolate(temperature, qn_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t dfe1::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t dfe1::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t dfe1::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
	
	unit::energy_t dfe1::qn_pos_dissipation()
	{
		return interpolate(temperature, qn_pos_energy);
	}
	
	unit::energy_t dfe1::qn_neg_dissipation()
	{
		return interpolate(temperature, qn_neg_energy);
	}
	
	unit::energy_t dfe1::d_pos_dissipation() //itt figyelni kell, hogy csak akkor adjon vissza nem nullat, ha E==1 es Q!=D
	{
		if((E.read() == SC_LOGIC_1) && (Q.read() != D.read()))
		{
			return interpolate(temperature, sample_pos_energy);
		}
		return 0.0_J;
	}
	
	unit::energy_t dfe1::d_neg_dissipation() //itt figyelni kell, hogy csak akkor adjon vissza nem nullat, ha E==1 es Q!= D
	{
		if((E.read() == SC_LOGIC_1) && (Q.read() != D.read()))
		{
			return interpolate(temperature, sample_neg_energy);
		}
		return 0.0_J;
	}
	
	unit::energy_t dfe1::e_pos_dissipation() //itt figyelni kell, hogy lehet hogy csak szimplan e energiaja disszipalodik, de lehet, hogy beiras is van
	{
		unit::energy_t result = interpolate(temperature, e_pos_energy);
		if(Q.read() != D.read())
		{
			result += interpolate(temperature, sample_pos_energy);
		}
		return result;
	}
	
	unit::energy_t dfe1::e_neg_dissipation()
	{
		unit::energy_t result = interpolate(temperature, e_neg_energy);
		if(Q.read() != D.read())
		{
			result += interpolate(temperature, sample_neg_energy);
		}
		return result;
	}
	
	unit::energy_t dfe1::c_pos_dissipation()
	{
		return interpolate(temperature, c_pos_energy);
	}
	
	unit::energy_t dfe1::c_neg_dissipation()
	{
		return interpolate(temperature, c_neg_energy);
	}
	
	dfe1::dfe1(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&dfe1::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&dfe1::q_neg_dissipation, this)),
		qn_pos("qn_pos",std::bind(&dfe1::qn_pos_dissipation, this)),
		qn_neg("qn_neg",std::bind(&dfe1::qn_neg_dissipation, this)),
		d_pos("d_pos",std::bind(&dfe1::d_pos_dissipation, this)),
		d_neg("d_neg",std::bind(&dfe1::d_neg_dissipation, this)),
		e_pos("e_pos",std::bind(&dfe1::e_pos_dissipation, this)),
		e_neg("e_neg",std::bind(&dfe1::e_neg_dissipation, this)),
		c_pos("c_pos",std::bind(&dfe1::c_pos_dissipation, this)),
		c_neg("c_neg",std::bind(&dfe1::c_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(24.5_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		
		SC_HAS_PROCESS(dfe1);
		
		q_delay(Q);
		qn_delay(QN);
		
		if(zero_delay)
		{		
			q_delay.set_delay_function(std::bind(&dfe1::q_delay_func, this));
			qn_delay.set_delay_function(std::bind(&dfe1::qn_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << C.pos(); // << rst;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
		
		SC_ACTIVITY(qn_pos);
		sensitive << QN.pos();
		dont_initialize();
		
		SC_ACTIVITY(qn_neg);
		sensitive << QN.neg();
		dont_initialize();
		
		SC_ACTIVITY(d_pos);
		sensitive << D.pos();
		dont_initialize();
		
		SC_ACTIVITY(d_neg);
		sensitive << D.neg();
		dont_initialize();
		
		SC_ACTIVITY(e_pos);
		sensitive << E.pos();
		dont_initialize();
		
		SC_ACTIVITY(e_neg);
		sensitive << E.neg();
		dont_initialize();
		
		SC_ACTIVITY(c_pos);
		sensitive << C.pos();
		dont_initialize();
		
		SC_ACTIVITY(c_neg);
		sensitive << C.neg();
		dont_initialize();
	}
	
	const unit::energy_t imux20::q_pos_energy[9]
	{	
		0.1787954479046714_pJ,
		0.1786496036102579_pJ,
		0.178555797017459_pJ,
		0.1785179793263914_pJ,
		0.1785198675771712_pJ,
		0.1785416346342895_pJ,
		0.1785995296956003_pJ,
		0.1786906967857847_pJ,
		0.1787987158578211_pJ
	};
	
	const unit::energy_t imux20::q_neg_energy[9]
	{
		0.1702215599498604_pJ,
		0.1702708962587549_pJ,
		0.1703571138001272_pJ,
		0.1704811734895808_pJ,
		0.1706376481811361_pJ,
		0.1708259749194111_pJ,
		0.1710403332241971_pJ,
		0.1712779707391267_pJ,
		0.1715373523310872_pJ
	};
	
	const unit::energy_t imux20::s_pos_energy[9]
	{	
		74.12520098079971_fJ,
		73.64311534976044_fJ,
		73.20793604923206_fJ,
		72.75515933442013_fJ,
		72.20533624148151_fJ,
		71.55441778424216_fJ,
		71.58099202507591_fJ,
		74.14684311970582_fJ,
		78.50083861023728_fJ
	};
	
	const unit::energy_t imux20::s_neg_energy[9]
	{
		71.05184162601326_fJ,
		70.80948902605019_fJ,
		70.60795067401952_fJ,
		70.45564848351008_fJ,
		70.3632205535713_fJ,
		70.30298173133488_fJ,
		70.28072814146304_fJ,
		70.28298801080644_fJ,
		70.28793115016036_fJ
	};
	
	const unit::current_t imux20::leakage_current[9]
	{
		7.63455847930087_pA,
		7.69734930681468_pA,
		7.813343510287265_pA,
		8.036728585015178_pA,
		8.465582061698229_pA,
		9.267810968490247_pA,
		10.71750752486804_pA,
		13.23617552912669_pA,
		17.4452008685323_pA
	};
	
	const unit::time_t imux20::q_pos_delay[9]
	{
		0.3762425606003_ns,
		0.3809850829999_ns,
		0.3855797271502_ns,
		0.3900740917305_ns,
		0.3944103633601_ns,
		0.3987047564000_ns,
		0.4028359839993_ns,
		0.4068570289201_ns,
		0.4107748919701_ns
	};
	
	const unit::time_t imux20::q_neg_delay[9]
	{
		0.1739007134095_ns,
		0.1802606342900_ns,
		0.1866559050894_ns,
		0.1930861311094_ns,
		0.1995389400895_ns,
		0.2060037904697_ns,
		0.2124836407394_ns,
		0.2189688058093_ns,
		0.2254529072096_ns
	};
	
	void imux20::function()
	{
		if(S.read() == SC_LOGIC_0)
		{
			q_delay.write(~A.read());
		}
		else if(S.read() == SC_LOGIC_1)
		{
			q_delay.write(~B.read());
		}
	}
	
	sc_time imux20::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t imux20::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t imux20::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t imux20::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
	
	unit::energy_t imux20::s_pos_dissipation()
	{
		return interpolate(temperature, s_pos_energy);
	}
	
	unit::energy_t imux20::s_neg_dissipation()
	{
		return interpolate(temperature, s_neg_energy);
	}
	
	imux20::imux20(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&imux20::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&imux20::q_neg_dissipation, this)),
		s_pos("s_pos",std::bind(&imux20::s_pos_dissipation, this)),
		s_neg("s_neg",std::bind(&imux20::s_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))		
	{
		
		SC_HAS_PROCESS(imux20);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&imux20::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << S;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
		
		SC_ACTIVITY(s_pos);
		sensitive << S.pos();
		dont_initialize();
		
		SC_ACTIVITY(s_neg);
		sensitive << S.neg();
		dont_initialize();
	}
	
	const unit::energy_t mux21::q_pos_energy[9]
	{	
		0.2312274714211301_pJ,
		0.2317444442857396_pJ,
		0.2323293142089916_pJ,
		0.2329842511037145_pJ,
		0.233700917766386_pJ,
		0.2344774616652083_pJ,
		0.2352996677197004_pJ,
		0.2361779501993481_pJ,
		0.2371049267976397_pJ
	};
	
	const unit::energy_t mux21::q_neg_energy[9]
	{
		0.2495435051288269_pJ,
		0.249995004107528_pJ,
		0.2505157866221216_pJ,
		0.251089122252786_pJ,
		0.2517421361737667_pJ,
		0.2524286212509798_pJ,
		0.2531854158887006_pJ,
		0.2539695164066215_pJ,
		0.2547967376085905_pJ
	};
	
	const unit::energy_t mux21::s_pos_energy[9]
	{	
		77.58705837993568_fJ,
		77.00321588738039_fJ,
		76.49019381022314_fJ,
		75.93369931310644_fJ,
		75.11151378943554_fJ,
		74.09181949207789_fJ,
		73.90011117042592_fJ,
		76.37251469930632_fJ,
		80.61513087748935_fJ
	};
	
	const unit::energy_t mux21::s_neg_energy[9]
	{
		71.71062793587691_fJ,
		71.38281919773076_fJ,
		71.07906767513465_fJ,
		70.81075982678922_fJ,
		70.60637464948917_fJ,
		70.4492644164827_fJ,
		70.34321739920118_fJ,
		70.30594984282511_fJ,
		70.31052893151123_fJ
	};
	
	const unit::current_t mux21::leakage_current[9]
	{
		10.9356490378083_pA,
		11.00073661178139_pA,
		11.12314103568971_pA,
		11.36321287350161_pA,
		11.83285496913799_pA,
		12.72925965736978_pA,
		14.38541393998386_pA,
		17.33581245896633_pA,
		22.40953044234376_pA
	};
	
	const unit::time_t mux21::q_pos_delay[9]
	{
		0.2582221067097_ns,
		0.2670094714495_ns,
		0.2757856636994_ns,
		0.2845552703498_ns,
		0.2933170187699_ns,
		0.3020572311997_ns,
		0.3107804980596_ns,
		0.3194541508094_ns,
		0.3280940869393_ns
	};
	
	const unit::time_t mux21::q_neg_delay[9]
	{
		0.4118790725598_ns,
		0.4187214461607_ns,
		0.4254228826796_ns,
		0.4320858938597_ns,
		0.4387200862295_ns,
		0.4453059008496_ns,
		0.4518192592608_ns,
		0.4582930938204_ns,
		0.4647349838699_ns
	};
	
	void mux21::function()
	{
		if(S.read() == SC_LOGIC_0)
		{
			q_delay.write(A.read());
		}
		else if(S.read() == SC_LOGIC_1)
		{
			q_delay.write(B.read());
		}
	}
	
	sc_time mux21::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t mux21::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t mux21::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t mux21::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
	
	unit::energy_t mux21::s_pos_dissipation()
	{
		return interpolate(temperature, s_pos_energy);
	}
	
	unit::energy_t mux21::s_neg_dissipation()
	{
		return interpolate(temperature, s_neg_energy);
	}
	
	mux21::mux21(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&mux21::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&mux21::q_neg_dissipation, this)),
		s_pos("s_pos",std::bind(&mux21::s_pos_dissipation, this)),
		s_neg("s_neg",std::bind(&mux21::s_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(7_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		
		SC_HAS_PROCESS(mux21);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&mux21::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << S;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
		
		SC_ACTIVITY(s_pos);
		sensitive << S.pos();
		dont_initialize();
		
		SC_ACTIVITY(s_neg);
		sensitive << S.neg();
		dont_initialize();
	}
	
	const unit::energy_t mux31::q_pos_energy[9]
	{	
		0.3683267429737303_pJ,
		0.3693842582061094_pJ,
		0.3705349627314764_pJ,
		0.3717808919349365_pJ,
		0.3730900810501094_pJ,
		0.3744723562997868_pJ,
		0.3759345133264519_pJ,
		0.3774568221903518_pJ,
		0.3790223383741051_pJ
	};
	
	const unit::energy_t mux31::q_neg_energy[9]
	{
		0.3790741058653123_pJ,
		0.380109104008555_pJ,
		0.3812118702555351_pJ,
		0.3824389164974466_pJ,
		0.3837098480167637_pJ,
		0.3850601376177198_pJ,
		0.3864748862914891_pJ,
		0.3879330604158149_pJ,
		0.3894476300522711_pJ
	};
	
	const unit::energy_t mux31::s0_pos_energy[9]
	{	
		86.36209588738709_fJ,
		85.72208551263665_fJ,
		85.13601620869536_fJ,
		84.5696321531256_fJ,
		83.72276932622957_fJ,
		83.68160558087444_fJ,
		86.2994948338001_fJ,
		90.88392454659248_fJ,
		96.58049167107399_fJ
	};
	
	const unit::energy_t mux31::s0_neg_energy[9]
	{
		79.42111349538194_fJ,
		79.17579226774867_fJ,
		78.90982772200628_fJ,
		78.73328329729225_fJ,
		78.47986928775474_fJ,
		78.35226718475187_fJ,
		78.1852539142717_fJ,
		78.04052776577001_fJ,
		77.88269230790009_fJ
	};
	
	const unit::energy_t mux31::s1_pos_energy[9]
	{	
		64.70908852007296_fJ,
		64.29465373430001_fJ,
		63.86721104133629_fJ,
		63.41431357596702_fJ,
		62.81454606736138_fJ,
		61.92272849049863_fJ,
		62.58893668275621_fJ,
		65.54413655919099_fJ,
		70.02397954654883_fJ
	};
	
	const unit::energy_t mux31::s1_neg_energy[9]
	{
		57.43393518092705_fJ,
		57.39216669507049_fJ,
		57.35540966155923_fJ,
		57.33943010013046_fJ,
		57.31906413520785_fJ,
		57.35247617771351_fJ,
		57.34659083507821_fJ,
		57.3607000949793_fJ,
		57.38603498944779_fJ
	};
	
	const unit::current_t mux31::leakage_current[9]
	{
		15.90773911694857_pA,
		15.99938491891793_pA,
		16.18093974305329_pA,
		16.546226208271_pA,
		17.26723721359956_pA,
		18.64248032974644_pA,
		21.16409490964807_pA,
		25.59911659769518_pA,
		33.10908811069041_pA
	};
	
	const unit::time_t mux31::q_pos_delay[9]
	{
		0.3458893699295_ns,
		0.3574206983898_ns,
		0.3689717232800_ns,
		0.3805294456495_ns,
		0.3920902290198_ns,
		0.4036341229005_ns,
		0.4151670246500_ns,
		0.4266559246905_ns,
		0.4381113046099_ns
	};
	
	const unit::time_t mux31::q_neg_delay[9]
	{
		0.4950264916397_ns,
		0.5045624667097_ns,
		0.5139666415303_ns,
		0.5233429670207_ns,
		0.5326485494602_ns,
		0.5419083936503_ns,
		0.5511147659007_ns,
		0.5601748301806_ns,
		0.5692055821899_ns
	};
	
	void mux31::function()
	{
		if(S1.read() == SC_LOGIC_0)
		{
			if(S0.read() == SC_LOGIC_0)
			{
				q_delay.write(A.read());
			}
			else if(S0.read() == SC_LOGIC_1)
			{
				q_delay.write(B.read());
			}
		}
		else if(S1.read() == SC_LOGIC_1)
		{
			q_delay.write(C.read());
		}
	}
	
	sc_time mux31::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t mux31::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t mux31::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t mux31::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
	
	unit::energy_t mux31::s0_pos_dissipation()
	{
		return interpolate(temperature, s0_pos_energy);
	}
	
	unit::energy_t mux31::s0_neg_dissipation()
	{
		return interpolate(temperature, s0_neg_energy);
	}
	
	unit::energy_t mux31::s1_pos_dissipation()
	{
		return interpolate(temperature, s1_pos_energy);
	}
	
	unit::energy_t mux31::s1_neg_dissipation()
	{
		return interpolate(temperature, s1_neg_energy);
	}
	
	mux31::mux31(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&mux31::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&mux31::q_neg_dissipation, this)),
		s0_pos("s0_pos",std::bind(&mux31::s0_pos_dissipation, this)),
		s0_neg("s0_neg",std::bind(&mux31::s0_neg_dissipation, this)),
		s1_pos("s1_pos",std::bind(&mux31::s1_pos_dissipation, this)),
		s1_neg("s1_neg",std::bind(&mux31::s1_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(14_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		
		SC_HAS_PROCESS(mux31);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&mux31::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << C << S0 << S1;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
		
		SC_ACTIVITY(s0_pos);
		sensitive << S0.pos();
		dont_initialize();
		
		SC_ACTIVITY(s0_neg);
		sensitive << S0.neg();
		dont_initialize();
		
		SC_ACTIVITY(s1_pos);
		sensitive << S1.pos();
		dont_initialize();
		
		SC_ACTIVITY(s1_neg);
		sensitive << S1.neg();
		dont_initialize();
	}
	
	const unit::energy_t mux41::q_pos_energy[9]
	{	
		0.3672755009324135_pJ,
		0.3683263895098689_pJ,
		0.3694747117491026_pJ,
		0.3707058161669524_pJ,
		0.3720042361467652_pJ,
		0.3734018750893659_pJ,
		0.374852269000813_pJ,
		0.3763556031121226_pJ,
		0.3779240109035199_pJ
	};
	
	const unit::energy_t mux41::q_neg_energy[9]
	{
		0.3838308030323503_pJ,
		0.3848901652716792_pJ,
		0.3860303238199421_pJ,
		0.3872730271295779_pJ,
		0.3885709572406038_pJ,
		0.3899513760137145_pJ,
		0.3913888599507801_pJ,
		0.3928641244580554_pJ,
		0.3943951337832592_pJ
	};
	
	const unit::energy_t mux41::s0_pos_energy[9]
	{	
		0.1597740553656761_pJ,
		0.1586703196428004_pJ,
		0.1575067894565874_pJ,
		0.1564362530485861_pJ,
		0.1561900535016642_pJ,
		0.1588739084823789_pJ,
		0.1645493236128198_pJ,
		0.1739347553310929_pJ,
		0.1842768689276373_pJ
	};
	
	const unit::energy_t mux41::s0_neg_energy[9]
	{
		0.1490210096370577_pJ,
		0.1485356163422527_pJ,
		0.1481495886986836_pJ,
		0.1477853473997098_pJ,
		0.1474519840667934_pJ,
		0.1471027464249401_pJ,
		0.1467960830887941_pJ,
		0.1464985333078098_pJ,
		0.1462390928060453_pJ
	};
	
	const unit::energy_t mux41::s1_pos_energy[9]
	{	
		69.71701583266263_fJ,
		69.11592737079339_fJ,
		68.55949185846859_fJ,
		68.1408950996305_fJ,
		67.68073552143039_fJ,
		67.29382266230179_fJ,
		67.01726844373888_fJ,
		66.73170288424129_fJ,
		66.48153343795801_fJ
	};
	
	const unit::energy_t mux41::s1_neg_energy[9]
	{
		66.32612684978245_fJ,
		66.11135526034144_fJ,
		65.98031469846277_fJ,
		65.85533223432806_fJ,
		65.7623441436606_fJ,
		65.71308123891171_fJ,
		65.70760541193424_fJ,
		65.74662455826281_fJ,
		65.82315889794593_fJ,
	};
	
	const unit::current_t mux41::leakage_current[9]
	{
		18.5849321756077_pA,
		18.72318179477915_pA,
		18.98529667163952_pA,
		19.50156174924953_pA,
		20.51158377860721_pA,
		22.43291471576124_pA,
		25.95908104978326_pA,
		32.18001508320353_pA,
		42.77494907834236_pA
	};
	
	const unit::time_t mux41::q_pos_delay[9]
	{
		0.3467517323605_ns,
		0.3582394122008_ns,
		0.3697575717103_ns,
		0.3812836990698_ns,
		0.3928030907099_ns,
		0.4043109306603_ns,
		0.4158019291402_ns,
		0.4272631087196_ns,
		0.4386733102801_ns
	};
	
	const unit::time_t mux41::q_neg_delay[9]
	{
		0.5000291778899_ns,
		0.5096517978599_ns,
		0.5192087002206_ns,
		0.5286979606807_ns,
		0.5381924107202_ns,
		0.5475200845202_ns,
		0.5568575381803_ns,
		0.5660146126498_ns,
		0.5751783793403_ns
	};
	
	void mux41::function()
	{
		if(S1.read() == SC_LOGIC_0)
		{
			if(S0.read() == SC_LOGIC_0)
			{
				q_delay.write(A.read());
			}
			else if(S0.read() == SC_LOGIC_1)
			{
				q_delay.write(B.read());
			}
		}
		else if(S1.read() == SC_LOGIC_1)
		{
			if(S0.read() == SC_LOGIC_0)
			{
				q_delay.write(C.read());
			}
			else if(S0.read() == SC_LOGIC_1)
			{
				q_delay.write(D.read());
			}
		}
	}
	
	sc_time mux41::q_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, q_pos_delay), interpolate(temperature, q_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t mux41::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
	
	unit::energy_t mux41::q_pos_dissipation()
	{
		return interpolate(temperature, q_pos_energy);
	}
	
	unit::energy_t mux41::q_neg_dissipation()
	{
		return interpolate(temperature, q_neg_energy);
	}
	
	unit::energy_t mux41::s0_pos_dissipation()
	{
		return interpolate(temperature, s0_pos_energy);
	}
	
	unit::energy_t mux41::s0_neg_dissipation()
	{
		return interpolate(temperature, s0_neg_energy);
	}
	
	unit::energy_t mux41::s1_pos_dissipation()
	{
		return interpolate(temperature, s1_pos_energy);
	}
	
	unit::energy_t mux41::s1_neg_dissipation()
	{
		return interpolate(temperature, s1_neg_energy);
	}
	
	mux41::mux41(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		q_pos("q_pos",std::bind(&mux41::q_pos_dissipation, this)),
		q_neg("q_neg",std::bind(&mux41::q_neg_dissipation, this)),
		s0_pos("s0_pos",std::bind(&mux41::s0_pos_dissipation, this)),
		s0_neg("s0_neg",std::bind(&mux41::s0_neg_dissipation, this)),
		s1_pos("s1_pos",std::bind(&mux41::s1_pos_dissipation, this)),
		s1_neg("s1_neg",std::bind(&mux41::s1_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(17.5_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		SC_HAS_PROCESS(mux41);
		
		q_delay(Q);
		
		if(zero_delay)
		{
			q_delay.set_delay_function(std::bind(&mux41::q_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << C << D << S0 << S1;
		dont_initialize();
		
		SC_ACTIVITY(q_pos);
		sensitive << Q.pos();
		dont_initialize();
		
		SC_ACTIVITY(q_neg);
		sensitive << Q.neg();
		dont_initialize();
		
		SC_ACTIVITY(s0_pos);
		sensitive << S0.pos();
		dont_initialize();
		
		SC_ACTIVITY(s0_neg);
		sensitive << S0.neg();
		dont_initialize();
		
		SC_ACTIVITY(s1_pos);
		sensitive << S1.pos();
		dont_initialize();
		
		SC_ACTIVITY(s1_neg);
		sensitive << S1.neg();
		dont_initialize();
	}
	
	const unit::energy_t add31::s_pos_energy[9]
	{	
		0.2668676184023016_pJ,
		0.2684338597357656_pJ,
		0.2700470058091657_pJ,
		0.2717242886267864_pJ,
		0.2734562383711697_pJ,
		0.2752290376437562_pJ,
		0.2770180159820157_pJ,
		0.2788447237669571_pJ,
		0.2806801956284967_pJ
	};
	
	const unit::energy_t add31::s_neg_energy[9]
	{
		0.3730751282824992_pJ,
		0.3750152751050612_pJ,
		0.3770096525895568_pJ,
		0.3790306254425837_pJ,
		0.3811309043081204_pJ,
		0.383326332796307_pJ,
		0.3855374331908733_pJ,
		0.3877726269851294_pJ,
		0.390057569994087_pJ
	};
	
	const unit::energy_t add31::co_pos_energy[9]
	{	
		0.5028388087095614_pJ,
		0.5046002065341223_pJ,
		0.5064782420232992_pJ,
		0.5085543804807983_pJ,
		0.5107985839957739_pJ,
		0.5130823357072671_pJ,
		0.5154364546822507_pJ,
		0.5179907498882981_pJ,
		0.5205668928490812_pJ
	};
	
	const unit::energy_t add31::co_neg_energy[9]
	{
		0.5037883693995804_pJ,
		0.5044506509088952_pJ,
		0.505276127783949_pJ,
		0.5062133859204476_pJ,
		0.5072532099567712_pJ,
		0.5083935808413448_pJ,
		0.5096347098664871_pJ,
		0.5109665032443_pJ,
		0.51239411646176_pJ
	};
	
	const unit::current_t add31::leakage_current[9]
	{
		19.47869289542664_pA,
		19.61503382444634_pA,
		19.85377353696985_pA,
		20.30441579000468_pA,
		21.17082187773869_pA,
		22.81132106164718_pA,
		25.82662071639252_pA,
		31.16248026897374_pA,
		41.13320976078305_pA
	};
	
	const unit::time_t add31::s_pos_delay[9]
	{
		0.3190833942399_ns,
		0.3296981721802_ns,
		0.3403415076401_ns,
		0.3510092386700_ns,
		0.3616918240801_ns,
		0.3723755067301_ns,
		0.3830417967501_ns,
		0.3936752636101_ns,
		0.4042513356500_ns
	};
	
	const unit::time_t add31::s_neg_delay[9]
	{
		0.5953913055700_ns,
		0.6085919648599_ns,
		0.6217285232801_ns,
		0.6347595814403_ns,
		0.6477230485100_ns,
		0.6606023311902_ns,
		0.6733305131701_ns,
		0.6859736954701_ns,
		0.6986144057801_ns
	};
	
	const unit::time_t add31::co_pos_delay[9]
	{
		0.4391138298898_ns,
		0.4506271819397_ns,
		0.4621019343196_ns,
		0.4735280702397_ns,
		0.4848996833902_ns,
		0.4962206282800_ns,
		0.5074839333299_ns,
		0.5186759112498_ns,
		0.5297806258297_ns
	};
	
	const unit::time_t add31::co_neg_delay[9]
	{
		0.2540374648100_ns,
		0.2583622276106_ns,
		0.2626357884300_ns,
		0.2668557470402_ns,
		0.2710232227801_ns,
		0.2751323024998_ns,
		0.2792004658907_ns,
		0.2832061363605_ns,
		0.2871680017701_ns
	};
	
	void add31::function()
	{
		s_delay.write((A.read() ^ B.read()) ^ CI.read());
		co_delay.write((A.read() & B.read()) | (CI.read() & (A.read() ^ B.read())));
	}
	
	sc_time add31::s_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, s_pos_delay), interpolate(temperature, s_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	sc_time add31::co_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, co_pos_delay), interpolate(temperature, co_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t add31::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
			
	unit::energy_t add31::s_pos_dissipation()
	{
		return interpolate(temperature, s_pos_energy);
	}
	
	unit::energy_t add31::s_neg_dissipation()
	{
		return interpolate(temperature, s_neg_energy);
	}
	
	unit::energy_t add31::co_pos_dissipation()
	{
		return interpolate(temperature, co_pos_energy);
	}
	
	unit::energy_t add31::co_neg_dissipation()
	{
		return interpolate(temperature, co_neg_energy);
	}
	
	add31::add31(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		s_pos("s_pos",std::bind(&add31::s_pos_dissipation, this)),
		s_neg("s_neg",std::bind(&add31::s_neg_dissipation, this)),
		co_pos("co_pos",std::bind(&add31::co_pos_dissipation, this)),
		co_neg("co_neg",std::bind(&add31::co_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(24_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		
		SC_HAS_PROCESS(add31);
		
		s_delay(S);
		co_delay(CO);
		
		if(zero_delay)
		{
			s_delay.set_delay_function(std::bind(&add31::s_delay_func, this));
			co_delay.set_delay_function(std::bind(&add31::co_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B << CI;
		dont_initialize();
		
		SC_ACTIVITY(s_pos);
		sensitive << S.pos();
		dont_initialize();
		
		SC_ACTIVITY(s_neg);
		sensitive << S.neg();
		dont_initialize();
		
		SC_ACTIVITY(co_pos);
		sensitive << CO.pos();
		dont_initialize();
		
		SC_ACTIVITY(co_neg);
		sensitive << CO.neg();
		dont_initialize();
	}
	
	const unit::energy_t add21::s_pos_energy[9]
	{	
		0.1830418283626498_pJ,
		0.1828109976098579_pJ,
		0.1825990767083685_pJ,
		0.1823811959748896_pJ,
		0.182229047325784_pJ,
		0.182154410503224_pJ,
		0.1821452290050779_pJ,
		0.1822320059263766_pJ,
		0.1824005077415838_pJ
	};
	
	const unit::energy_t add21::s_neg_energy[9]
	{
		0.2465902621121297_pJ,
		0.2470671505554064_pJ,
		0.2475933854354474_pJ,
		0.2481958097052141_pJ,
		0.2488321477851324_pJ,
		0.2495204070513811_pJ,
		0.2502556413252601_pJ,
		0.2510328839807727_pJ,
		0.2518562558169454_pJ
	};
	
	const unit::energy_t add21::co_pos_energy[9]
	{	
		0.2396430172353546_pJ,
		0.2398923772386719_pJ,
		0.2401941370819078_pJ,
		0.2405288659657192_pJ,
		0.2409111058673279_pJ,
		0.2413208383199457_pJ,
		0.2417656555717458_pJ,
		0.2422316931008159_pJ,
		0.2427292004570049_pJ
	};
	
	const unit::energy_t add21::co_neg_energy[9]
	{
		0.2946011951367218_pJ,
		0.2949919163806323_pJ,
		0.2955059317308688_pJ,
		0.296066714530213_pJ,
		0.2966914066000808_pJ,
		0.2973668985398559_pJ,
		0.2981129351828164_pJ,
		0.2989191786773686_pJ,
		0.2997600720492136_pJ
	};
	
	const unit::current_t add21::leakage_current[9]
	{
		13.55292419666271_pA,
		13.64012801730582_pA,
		13.78968272592049_pA,
		14.07089132821861_pA,
		14.61464736661728_pA,
		15.65795356279968_pA,
		17.61475716915511_pA,
		21.17265863547337_pA,
		27.44136438459478_pA
	};
	
	const unit::time_t add21::s_pos_delay[9]
	{
		0.1912763665500_ns,
		0.1986080488101_ns,
		0.2059007769700_ns,
		0.2131526909901_ns,
		0.2203696098001_ns,
		0.2275303923801_ns,
		0.2346384700801_ns,
		0.2416814029801_ns,
		0.2486515370199_ns
	};
	
	const unit::time_t add21::s_neg_delay[9]
	{
		0.4284442006899_ns,
		0.4344583311499_ns,
		0.4404229965702_ns,
		0.4463177396396_ns,
		0.4521017580200_ns,
		0.4578565001398_ns,
		0.4635147207999_ns,
		0.4691470170197_ns,
		0.4746679604099_ns
	};
	
	const unit::time_t add21::co_pos_delay[9]
	{
		0.29056062829969_ns,
		0.299097315120214_ns,
		0.307572721549848_ns,
		0.315985370679921_ns,
		0.324333004990599_ns,
		0.332613881699735_ns,
		0.34082108053968_ns,
		0.348941370029939_ns,
		0.35699944485959_ns
	};
	
	const unit::time_t add21::co_neg_delay[9]
	{
		0.287550764020083_ns,
		0.29117335788061_ns,
		0.294724521100329_ns,
		0.298219701860203_ns,
		0.301652162949955_ns,
		0.305023101029781_ns,
		0.308333136929939_ns,
		0.311582500230238_ns,
		0.314776650730568_ns
	};
	
	void add21::function()
	{
		s_delay.write((A.read() ^ B.read()));
		co_delay.write((A.read() & B.read()));
	}
	
	sc_time add21::s_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, s_pos_delay), interpolate(temperature, s_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	sc_time add21::co_delay_func()
	{
		unit::time_t delay = std::max(interpolate(temperature, co_pos_delay), interpolate(temperature, co_neg_delay));
		return sc_time(static_cast<double>(delay),SC_SEC);
	}
	
	unit::power_t add21::component_static_dissipation() const
	{
		return 3.3_V * interpolate(temperature, leakage_current);
	}
			
	unit::energy_t add21::s_pos_dissipation()
	{
		return interpolate(temperature, s_pos_energy);
	}
	
	unit::energy_t add21::s_neg_dissipation()
	{
		return interpolate(temperature, s_neg_energy);
	}
	
	unit::energy_t add21::co_pos_dissipation()
	{
		return interpolate(temperature, co_pos_energy);
	}
	
	unit::energy_t add21::co_neg_dissipation()
	{
		return interpolate(temperature, co_neg_energy);
	}
	
	add21::add21(const sc_module_name& nm, const std::string& layer_name, const layout::xy_length_t& position, layout::sunred::component_t* parent, logic::systemc::adapter_t* logic_adapter)
	:
		sc_core::sc_module(nm, logic_adapter),
		s_pos("s_pos",std::bind(&add21::s_pos_dissipation, this)),
		s_neg("s_neg",std::bind(&add21::s_neg_dissipation, this)),
		co_pos("co_pos",std::bind(&add21::co_pos_dissipation, this)),
		co_neg("co_neg",std::bind(&add21::co_neg_dissipation, this)),
		layout_item(id, layer_name, position, layout::xy_length_t(10.5_um,13_um), parent, dynamic_cast<layout::sunred::adapter_t*>(parent->adapter))
	{
		
		SC_HAS_PROCESS(add21);
		
		s_delay(S);
		co_delay(CO);
		
		if(zero_delay)
		{
			s_delay.set_delay_function(std::bind(&add21::s_delay_func, this));
			co_delay.set_delay_function(std::bind(&add21::co_delay_func, this));
		}
		
		SC_METHOD(function);
		sensitive << A << B;
		dont_initialize();
		
		SC_ACTIVITY(s_pos);
		sensitive << S.pos();
		dont_initialize();
		
		SC_ACTIVITY(s_neg);
		sensitive << S.neg();
		dont_initialize();
		
		SC_ACTIVITY(co_pos);
		sensitive << CO.pos();
		dont_initialize();
		
		SC_ACTIVITY(co_neg);
		sensitive << CO.neg();
		dont_initialize();
	}
}
